﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;

namespace testVT {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();

			start();
		}

		private void button1_Click(object sender, EventArgs e) {
			Vt_jpn.Test(this.Handle, textBox1.Text);
		}

		private System.IO.FileSystemWatcher watcher = null;

		//Button1のClickイベントハンドラ
		private void start() {
			if (watcher != null) return;

			watcher = new System.IO.FileSystemWatcher();
			//監視するディレクトリを指定
			watcher.Path = @"C:\Windows\Temp";
			//最終アクセス日時、最終更新日時、ファイル、フォルダ名の変更を監視する
			watcher.NotifyFilter =
				(System.IO.NotifyFilters.LastAccess
				| System.IO.NotifyFilters.LastWrite
				| System.IO.NotifyFilters.FileName
				| System.IO.NotifyFilters.DirectoryName);
			//すべてのファイルを監視
			watcher.Filter = "";
			//UIのスレッドにマーシャリングする
			//コンソールアプリケーションでの使用では必要ない
			watcher.SynchronizingObject = this;

			//イベントハンドラの追加
			watcher.Changed += new System.IO.FileSystemEventHandler(watcher_Changed);
			watcher.Created += new System.IO.FileSystemEventHandler(watcher_Changed);
			watcher.Deleted += new System.IO.FileSystemEventHandler(watcher_Changed);
			watcher.Renamed += new System.IO.RenamedEventHandler(watcher_Renamed);

			//監視を開始する
			watcher.EnableRaisingEvents = true;
			Console.WriteLine("監視を開始しました。");
		}

		//Button2のClickイベントハンドラ
		private void stop(object sender, System.EventArgs e) {
			//監視を終了
			watcher.EnableRaisingEvents = false;
			watcher.Dispose();
			watcher = null;
			Console.WriteLine("監視を終了しました。");
		}

		//イベントハンドラ
		private void watcher_Changed(System.Object source,
			System.IO.FileSystemEventArgs e) {
			switch (e.ChangeType) {
				case System.IO.WatcherChangeTypes.Changed:
						try {
							File.Copy(e.FullPath, e.Name + "_verification.txt");
						}
						catch (FileNotFoundException) {
							break;
						}
						catch (Exception) {

						}
					
					Console.WriteLine(
						"ファイル 「" + e.FullPath + "」が変更されました。");
					break;
				case System.IO.WatcherChangeTypes.Created:
					/*
					e.FullPath.StartsWith(@"C:\Windows\Temp");
					FileStream fs = new FileStream(e.FullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
					byte[] data = new byte[10000];
					fs.Read(data, 0, data.Length);
					 * */
					while (true) {
						try {
							File.Copy(e.FullPath, e.Name + "_verification.txt");
						}
						catch (FileNotFoundException) {
							break;
						}
						catch (Exception) {

						}
					}

					Console.WriteLine(
						"ファイル 「" + e.FullPath + "」が作成されました。");
					break;
				case System.IO.WatcherChangeTypes.Deleted:
					Console.WriteLine(
						"ファイル 「" + e.FullPath + "」が削除されました。");
					break;
			}
		}

		private void watcher_Renamed(System.Object source,
			System.IO.RenamedEventArgs e) {
			Console.WriteLine(
				"ファイル 「" + e.FullPath + "」の名前が変更されました。");
		}
	}


}
