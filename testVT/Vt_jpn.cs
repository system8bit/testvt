﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using System.Media;

using System.IO;

namespace testVT {
	class Vt_jpn {
		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern short VT_LOADTTS_JPN(IntPtr hWnd, int nSpeakerID, string db_path, string licensefile);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern void VT_UNLOADTTS_JPN(int nSpeakerID);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern short VT_LOAD_UserDict_JPN(int dictidx, string filename);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern short VT_UNLOAD_UserDict_JPN(int dictidx);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern short VT_PLAYTTS_JPN(IntPtr hcaller, uint umsg, string text_buff, int nSpeakerID, int pitch, int speed, int volume, int pause, int dictidx, int texttype);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern void VT_STOPTTS_JPN();

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern void VT_PAUSETTS_JPN();

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern void VT_RESTARTTTS_JPN();

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern short VT_TextToFile_JPN(int fmt, string tts_text, string filename, int nSpeakerID, int pitch, int speed, int volume, int pause, int dictidx, int texttype);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.None)]
		static extern int VT_TextToBuffer_JPN(int fmt, string tts_text, ushort[] output_buff, ref int output_len, int flag, int nThreadID, int nSpeakerID, int pitch, int speed, int volume, int pause, int dictidx, int texttype);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern void VT_SetPitchSpeedVolumePause_JPN ( int pitch, int speed, int volume, int pause, int nSpeakerID);

		[DllImport(@"C:\Program Files (x86)\TAKARATOMY\Twimal\vt_jpn.dll", CallingConvention = CallingConvention.Cdecl)]
		static extern int VT_GetTTSInfo_JPN (int request, string licensefile, ref int value, int valuesize);

		const int VT_LOADTTS_SUCCESS = 0;
		const int VT_LOADTTS_ERROR_CONFLICT_DBPATH = 1;
		const int VT_LOADTTS_ERROR_TTS_STRUCTURE = 2;
		const int VT_LOADTTS_ERROR_TAGGER = 3;
		const int VT_LOADTTS_ERROR_BREAK_INDEX = 4;
		const int VT_LOADTTS_ERROR_TPP_DICT = 5;
		const int VT_LOADTTS_ERROR_TABLE = 6;
		const int VT_LOADTTS_ERROR_UNIT_INDEX = 7;
		const int VT_LOADTTS_ERROR_PROSODY_DB = 8;
		const int VT_LOADTTS_ERROR_PCM_DB = 9;
		const int VT_LOADTTS_ERROR_PM_DB = 10;
		const int VT_LOADTTS_ERROR_UNKNOWN = 11;

		const int VT_LOAD_USERDICT_SUCCESS =1;
		const int VT_LOAD_USERDICT_ERROR_INVALID_INDEX =-1;
		const int VT_LOAD_USERDICT_ERROR_INDEX_BUSY =-2;
		const int VT_LOAD_USERDICT_ERROR_LOAD_FAIL =-3;
		const int VT_LOAD_USERDICT_ERROR_UNKNOWN =-4;
		const int VT_UNLOAD_USERDICT_SUCCESS =1;
		const int VT_UNLOAD_USERDICT_ERROR_NULL_INDEX =-1;
		const int VT_UNLOAD_USERDICT_ERROR_INVALID_INDEX =-2;
		const int VT_UNLOAD_USERDICT_ERROR_UNKNOWN =-3;

		const int VT_PLAY_API_SUCCESS =1;
		const int VT_PLAY_API_ERROR_CREATE_THREAD =-1;
		const int VT_PLAY_API_ERROR_NULL_TEXT =-2;
		const int VT_PLAY_API_ERROR_EMPTY_TEXT =-3;
		const int VT_PLAY_API_ERROR_DB_NOT_LOADED =-4;
		const int VT_PLAY_API_ERROR_INITPLAY =-5;
		const int VT_PLAY_API_ERROR_UNKNOWN =-6;

		const int VT_FILE_API_SUCCESS =1;
		const int VT_FILE_API_ERROR_INVALID_FORMAT =-1;
		const int VT_FILE_API_ERROR_CREATE_THREAD =-2;
		const int VT_FILE_API_ERROR_NULL_TEXT =-3;
		const int VT_FILE_API_ERROR_EMPTY_TEXT =-4;
		const int VT_FILE_API_ERROR_DB_NOT_LOADED =-5;
		const int VT_FILE_API_ERROR_OUT_FILE_OPEN =-6;
		const int VT_FILE_API_ERROR_UNKNOWN =-7;

		const int VT_FILE_API_FMT_S16PCM = 0;
		const int VT_FILE_API_FMT_ALAW = 1;
		const int VT_FILE_API_FMT_MULAW = 2;
		const int VT_FILE_API_FMT_DADPCM = 3;
		const int VT_FILE_API_FMT_S16PCM_WAVE = 4;
		const int VT_FILE_API_FMT_U08PCM_WAVE = 5;
		//const int VT_FILE_API_FMT_IMA_WAVE = 6; /* not supported! */
		const int VT_FILE_API_FMT_ALAW_WAVE = 7;
		const int VT_FILE_API_FMT_MULAW_WAVE = 8;
		const int VT_FILE_API_FMT_MULAW_AU = 9;

		const int VT_BUFFER_API_FMT_S16PCM = VT_FILE_API_FMT_S16PCM;
		const int VT_BUFFER_API_FMT_ALAW = VT_FILE_API_FMT_ALAW;
		const int VT_BUFFER_API_FMT_MULAW = VT_FILE_API_FMT_MULAW;
		const int VT_BUFFER_API_FMT_DADPCM = VT_FILE_API_FMT_DADPCM;

		const int VT_BUFFER_API_PROCESSING = 0;
		const int VT_BUFFER_API_DONE = 1;
		const int VT_BUFFER_API_ERROR_INVALID_FORMAT = -1;
		const int VT_BUFFER_API_ERROR_CREATE_THREAD = -2;
		const int VT_BUFFER_API_ERROR_NULL_TEXT = -3;
		const int VT_BUFFER_API_ERROR_EMPTY_TEXT = -4;
		const int VT_BUFFER_API_ERROR_NULL_BUFFER = -5;
		const int VT_BUFFER_API_ERROR_DB_NOT_LOADED = -6;
		const int VT_BUFFER_API_ERROR_THREAD_BUSY = -7;
		const int VT_BUFFER_API_ERROR_ABNORMAL_CONDITION = -8;
		const int VT_BUFFER_API_ERROR_UNKNOWN = -9;
		
		const int VT_INFO_SUCCESS = 0;
		const int VT_INFO_ERROR_NOT_SUPPORTED_REQUEST = 1;
		const int VT_INFO_ERROR_INVALID_REQUESTT = 2;
		const int VT_INFO_ERROR_NULL_VALUE = 3;
		const int VT_INFO_ERROR_SHORT_LENGTH_VALUE = 4;
		const int VT_INFO_ERROR_UNKNOWN = 5;

		const int VT_BUILD_DATE = 0;
		const int VT_VERIFY_CODE = 1;
		const int VT_MAX_CHANNEL = 2;
		const int VT_DB_DIRECTORY = 3;
		const int VT_LOAD_SUCCESS_CODE = 4;
		const int VT_MAX_SPEAKER = 5;
		const int VT_DEF_SPEAKER = 6;
		const int VT_CODEPAGE = 7;
		const int VT_DB_ACCESS_MODE = 8;
		const int VT_FIXED_POINT_SUPPORT= 9;
		const int VT_SAMPLING_FREQUENCY = 10;
		const int VT_MAX_PITCH_RATE = 11;
		const int VT_DEF_PITCH_RATE = 12;
		const int VT_MIN_PITCH_RATE = 13;
		const int VT_MAX_SPEED_RATE = 14;
		const int VT_DEF_SPEED_RATE = 15;
		const int VT_MIN_SPEED_RATE = 16;
		const int VT_MAX_VOLUME = 17;
		const int VT_DEF_VOLUME = 18;
		const int VT_MIN_VOLUME = 19;
		const int VT_MAX_SENT_PAUSE = 20;
		const int VT_DEF_SENT_PAUSE = 21;
		const int VT_MIN_SENT_PAUSE = 22;
		const int VT_DB_BUILD_DATE = 23;

		private struct WaveFmt {//WAVEファイルフォーマット
			public int freq;       //サンプリング周波数
			public int bits;       //量子化ビット
			public int channel;    //チャンネル
		}

		static private void writeWave(ushort[] pcm) {
			WaveFmt wavefmt;

			wavefmt.freq = 16000;
			wavefmt.bits = 16;
			wavefmt.channel = 1;

			uint data_size = (uint)pcm.Length * 2; //データサイズ
			int sec = (int)data_size / (wavefmt.freq * wavefmt.channel) ;    //再生時間(秒)
			ushort wFormatTag = 1;  //WAVE_FORMAT_PCM 0x0001
			ushort nChannels;
			uint nSamplesPerSec;
			uint nAvgBytesPerSec;
			ushort nBlockAlign;
			ushort wBitsPerSample;

			//data_size = (uint)(wavefmt.freq * sec * wavefmt.channel * 2);

			nChannels = (ushort)wavefmt.channel;
			nSamplesPerSec = (uint)wavefmt.freq;
			wBitsPerSample = (ushort)wavefmt.bits;
			nBlockAlign = (ushort)(wBitsPerSample / 8 * wavefmt.channel);
			nAvgBytesPerSec = nSamplesPerSec * nBlockAlign;

			// == WAVEヘッダ ==
			//
			// RIFF 識別子 4byte
			// 38(*1) + データサイズ 4byte
			// WAVE 識別子 4byte
			// fmt + 半角スペース 識別子 4byte
			// WAVEFORMATEXのサイズ[18 or 16] 4byte
			// WAVEFORMATEX(*2)
			// data 識別子 4byte
			// データサイズ 4byte
			// 以下データ
			//
			// -------------------
			//
			// *1:WAVEFORMATEXのサイズとWAVE識別子以下のサイズ。WAVEFORMATEXのcbSize[WORD:2byte]をカウントしない場合は36。
			// *2:WAVEFORMATEX構造体
			//    typedef struct { 
			//        WORD  wFormatTag;
			//        WORD  nChannels;
			//        DWORD nSamplesPerSec;
			//        DWORD nAvgBytesPerSec;
			//        WORD  nBlockAlign;
			//        WORD  wBitsPerSample;
			//        WORD  cbSize;
			//    } WAVEFORMATEX;
			//
			// == WAVEヘッダ ==

			FileStream fs;
			//ファイル名
			string SaveLocation = "test.wav";

			try {
				fs = new FileStream(SaveLocation, FileMode.Create);

				// ファイルストリームをBinaryWriterに関連付ける
				BinaryWriter bw = new BinaryWriter(fs);

				//WAVEヘッダ書き込み
				char[] s = new char[4];
				s[0] = 'R'; s[1] = 'I'; s[2] = 'F'; s[3] = 'F';
				bw.Write(s);
				uint size = 36 + data_size;
				bw.Write(size);
				s[0] = 'W'; s[1] = 'A'; s[2] = 'V'; s[3] = 'E';
				bw.Write(s);
				s[0] = 'f'; s[1] = 'm'; s[2] = 't'; s[3] = ' ';
				bw.Write(s);
				size = 16;
				bw.Write(size);
				bw.Write(wFormatTag);
				bw.Write(nChannels);
				bw.Write(nSamplesPerSec);
				bw.Write(nAvgBytesPerSec);
				bw.Write(nBlockAlign);
				bw.Write(wBitsPerSample);
				s[0] = 'd'; s[1] = 'a'; s[2] = 't'; s[3] = 'a';
				bw.Write(s);
				size = data_size;
				bw.Write(size);
				//WAVEヘッダ書き込み

				//データ書き込み
				for (int i = 0; i < pcm.Length; i++) {
					ushort waveL = pcm[i];
					ushort waveR;

					bw.Write(waveL);	//データ書き込み

					if (wavefmt.channel == 2) {//ステレオの場合
						//waveR = (ushort)(32768.0 * Math.Sin(2 * Math.PI * i * int.Parse(TextBox5.Text) / wavefmt.freq));
						//bw.Write(waveR);	//データ書き込み
					}
				}
				//データ書き込み

				bw.Close();
				fs.Close();

			}
			catch (System.IO.IOException) {

			}

		}

		static private void writeWave2(byte[] pcm) {
			WaveFmt wavefmt;

			wavefmt.freq = 16000;
			wavefmt.bits = 16;
			wavefmt.channel = 1;

			uint data_size = (uint)pcm.Length; //データサイズ
			int sec = (int)data_size / (wavefmt.freq * wavefmt.channel);    //再生時間(秒)
			ushort wFormatTag = 1;  //WAVE_FORMAT_PCM 0x0001
			ushort nChannels;
			uint nSamplesPerSec;
			uint nAvgBytesPerSec;
			ushort nBlockAlign;
			ushort wBitsPerSample;

			//data_size = (uint)(wavefmt.freq * sec * wavefmt.channel * 2);

			nChannels = (ushort)wavefmt.channel;
			nSamplesPerSec = (uint)wavefmt.freq;
			wBitsPerSample = (ushort)wavefmt.bits;
			nBlockAlign = (ushort)(wBitsPerSample / 8 * wavefmt.channel);
			nAvgBytesPerSec = nSamplesPerSec * nBlockAlign;

			// == WAVEヘッダ ==
			//
			// RIFF 識別子 4byte
			// 38(*1) + データサイズ 4byte
			// WAVE 識別子 4byte
			// fmt + 半角スペース 識別子 4byte
			// WAVEFORMATEXのサイズ[18 or 16] 4byte
			// WAVEFORMATEX(*2)
			// data 識別子 4byte
			// データサイズ 4byte
			// 以下データ
			//
			// -------------------
			//
			// *1:WAVEFORMATEXのサイズとWAVE識別子以下のサイズ。WAVEFORMATEXのcbSize[WORD:2byte]をカウントしない場合は36。
			// *2:WAVEFORMATEX構造体
			//    typedef struct { 
			//        WORD  wFormatTag;
			//        WORD  nChannels;
			//        DWORD nSamplesPerSec;
			//        DWORD nAvgBytesPerSec;
			//        WORD  nBlockAlign;
			//        WORD  wBitsPerSample;
			//        WORD  cbSize;
			//    } WAVEFORMATEX;
			//
			// == WAVEヘッダ ==

			FileStream fs;
			//ファイル名
			string SaveLocation = "test2.wav";

			try {
				fs = new FileStream(SaveLocation, FileMode.Create);

				// ファイルストリームをBinaryWriterに関連付ける
				BinaryWriter bw = new BinaryWriter(fs);

				//WAVEヘッダ書き込み
				char[] s = new char[4];
				s[0] = 'R'; s[1] = 'I'; s[2] = 'F'; s[3] = 'F';
				bw.Write(s);
				uint size = 36 + data_size;
				bw.Write(size);
				s[0] = 'W'; s[1] = 'A'; s[2] = 'V'; s[3] = 'E';
				bw.Write(s);
				s[0] = 'f'; s[1] = 'm'; s[2] = 't'; s[3] = ' ';
				bw.Write(s);
				size = 16;
				bw.Write(size);
				bw.Write(wFormatTag);
				bw.Write(nChannels);
				bw.Write(nSamplesPerSec);
				bw.Write(nAvgBytesPerSec);
				bw.Write(nBlockAlign);
				bw.Write(wBitsPerSample);
				s[0] = 'd'; s[1] = 'a'; s[2] = 't'; s[3] = 'a';
				bw.Write(s);
				size = data_size;
				bw.Write(size);
				//WAVEヘッダ書き込み

				//データ書き込み
				for (int i = 0; i < pcm.Length; i++) {
					byte waveL = pcm[i];
					byte waveR;

					bw.Write(waveL);	//データ書き込み

					if (wavefmt.channel == 2) {//ステレオの場合
						//waveR = (ushort)(32768.0 * Math.Sin(2 * Math.PI * i * int.Parse(TextBox5.Text) / wavefmt.freq));
						//bw.Write(waveR);	//データ書き込み
					}
				}
				//データ書き込み

				bw.Close();
				fs.Close();

			}
			catch (System.IO.IOException) {

			}
		}


		static public void Test(IntPtr hWnd, string text1) {
			string license = text1;
			int infoval = 0;
			if(VT_GetTTSInfo_JPN(VT_LOAD_SUCCESS_CODE, null, ref infoval, sizeof(int)) != VT_INFO_SUCCESS){
				return;
			}

			if (VT_LOADTTS_JPN(hWnd, -1, null, license) != infoval) {
				return;
			}

			if (VT_GetTTSInfo_JPN(VT_SAMPLING_FREQUENCY, null, ref infoval, sizeof(int)) != VT_INFO_SUCCESS) {
				return;
			}


			int flag = 0;
			int tid = 0;

			int frameflag;
			int nThreadID = 0;


			string text = "pentax";
			string fileName = "toFile.pcm";

			
			if (VT_TextToFile_JPN (VT_FILE_API_FMT_S16PCM , text, fileName, -1, -1, -1, -1, -1, -1, -1) != VT_FILE_API_SUCCESS) {
				VT_UNLOADTTS_JPN(-1);
				return;
			}
			

			int nFramesize = 0;
			int rc = frameflag = -1;
			if (VT_TextToBuffer_JPN (VT_BUFFER_API_FMT_S16PCM , null, null, ref nFramesize, frameflag, nThreadID, -1, -1, -1, -1, -1, -1, -1) < 0) {
				VT_UNLOADTTS_JPN (-1);
				return;
			}		


			int slen = 1000000;
			ushort[] sbuffer = new ushort[slen];
			rc = VT_TextToBuffer_JPN(VT_BUFFER_API_FMT_S16PCM, text, sbuffer, ref slen, flag, tid, -1, -1, -1, -1, -1, -1, -1);

			ushort[] pcm = new ushort[slen];
			Array.Copy(sbuffer, pcm, pcm.Length);

			writeWave(pcm);

			byte[] data = File.ReadAllBytes(fileName);

			writeWave2(data);

			
			VT_UNLOADTTS_JPN(-1);
			return;
		}
	}
}
